# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
((UKAZI GIT))
git clone https://dc9981@bitbucket.org/dc9981/stroboskop.git

git add js.color

git rm nepotrebno.js


git commit -a -m "Priprava potrebnih JavaScript knjižnic"

git push origin master
```

Naloga 6.2.3:
((UVELJAVITEV))

https://bitbucket.org/dc9981/stroboskop/commits/0327222e2b2b40466cd27cf0d2ed5bcd550ce4f0



## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
((UVELJAVITEV))

https://bitbucket.org/dc9981/stroboskop/commits/70ccf453660b93dc65ed969a26164ea86daef1f5


Naloga 6.3.2:
((UVELJAVITEV))

https://bitbucket.org/dc9981/stroboskop/commits/478a42292b235360fd9ce28da92a8c6dcb245f85


Naloga 6.3.3:
((UVELJAVITEV))

https://bitbucket.org/dc9981/stroboskop/commits/cbfa71071daa6553a111849ea02a907ee9471730

Naloga 6.3.4:
((UVELJAVITEV))

https://bitbucket.org/dc9981/stroboskop/commits/c8962f6d3525842b5a88b6a809c9f52957a3f288
```
spodnji link je popravek ker sem pozabil okvirju narediti rdec rob in sem kasneje popravil
```
https://bitbucket.org/dc9981/stroboskop/commits/071359dc89587aa97ae7b60f21cddaff1487167a


Naloga 6.3.5:

```
((UKAZI GIT))

git add .
git commit -a -m "Dodajanje stilov spletne strani"
git push origin izgled

git add .
git commit -a -m "Desna poravnava besedil"
git push origin izgled

git add .
git commit -a -m "Dodajanje gumba za dodajanje barv"
git push origin izgled

git add .
git commit -a -m "Dodajanje robov in senc"
git push origin izgled

git checkout master
git merge izgled
git push origin master

```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
((UVELJAVITEV))
https://bitbucket.org/dc9981/stroboskop/commits/2ab855b1fe5cf072c4de4f44e71219db15867b09

Naloga 6.4.2:
((UVELJAVITEV))
https://bitbucket.org/dc9981/stroboskop/commits/f3bc4f3394357f5f0b45ef3afcd0c3b0ff282e10

Naloga 6.4.3:
((UVELJAVITEV))
https://bitbucket.org/dc9981/stroboskop/commits/256d335e1eb6ffb3e2367a00eef9715f19144c53

Naloga 6.4.4:
((UVELJAVITEV))
https://bitbucket.org/dc9981/stroboskop/commits/10191ff3186723bacabb7e37660104f429a125e6